%global _empty_manifest_terminate_build 0
Name:           python-cachetools
Version:        5.5.0
Release:        1
Summary:        Extensible memoizing collections and decorators
License:        MIT
URL:            https://github.com/tkem/cachetools/
Source0:        https://files.pythonhosted.org/packages/c3/38/a0f315319737ecf45b4319a8cd1f3a908e29d9277b46942263292115eee7/cachetools-5.5.0.tar.gz
BuildArch:      noarch
%description
This module provides various memoizing collections and decorators,
including variants of the Python Standard Library's `@lru_cache`_
function decorator.

%package -n python3-cachetools
Summary:        Extensible memoizing collections and decorators
Provides:       python-cachetools
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-cachetools
This module provides various memoizing collections and decorators,
including variants of the Python Standard Library's `@lru_cache`_
function decorator.

%package help
Summary:        Extensible memoizing collections and decorators
Provides:       python3-cachetools-doc
%description help
This module provides various memoizing collections and decorators,
including variants of the Python Standard Library's `@lru_cache`_
function decorator.

%prep
%autosetup -n cachetools-%{version}

%build
%py3_build


%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} setup.py test

%files -n python3-cachetools -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Oct 11 2024 lixiaoyong <lixiaoyong@kylinos.cn> - 5.5.0-1
- Update to version 5.5.0
  - ``TTLCache.expire()`` returns iterable of expired ``(key, value)``
    pairs
  - ``TLRUCache.expire()`` returns iterable of expired ``(key, value)``
    pairs
  - Documentation improvements
  - Update CI environment

* Thu Jul 18 2024 liudy <liudingyao@kylinos.cn> - 5.4.0-1
- Update package to version 5.4.0
- Add the keys.typedmethodkey decorator 
- Deprecate MRUCache class
- Deprecate @func.mru_cache decorator
- Update CI environment

* Mon Nov 20 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 5.3.2-1
- Update package to version 5.3.2

* Tue Jun 27 2023 ZixuanChen <chenzixuan@kylinos.cn> - 5.3.1-1
- Upgrade to version 5.3.1

* Fri Apr 07 2023 xu_ping <707078654@qq.com> - 5.3.0-1
- Upgrade to version 5.3.0

* Thu Jun 23 2022 SimpleUpdate Robot <tc@openeuler.org> - 5.2.0-1
- Upgrade to version 5.2.0

* Tue Jul 13 2021 OpenStack_SIG <openstack@openeuler.org> - 4.2.1-1
- Upgrade to version 4.2.1

* Fri Oct 09 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
